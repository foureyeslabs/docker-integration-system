from concurrent.futures import ThreadPoolExecutor
from ostruct import OpenStruct
from uuid import uuid4

import os
import shutil
import subprocess

class BuildJob( object ):
  def __init__( self, job_info ):
    self.job_info = job_info
    self.commit_number = job_info[ "commit_number" ]
    self.group = job_info[ "group" ]
    self.app = job_info[ "app" ]
    self.git_url = job_info[ "git_url" ]

  def run( self ):
    self.__make_work_directory()
    response = {}
    response.update( self.job_info )

    clone_response = self.__clone_repo_into_dir()
    response.update( {"clone": clone_response} )

    if clone_response[ "status" ] != 0:
      return response

    build_response = self.__build_ci_docker()
    response.update( {"build": build_response} )


    if build_response[ "status" ] != 0:
      return response

    run_response = self.__run_ci_docker()
    response.update( {"run": run_response} )
    return response

  def __make_work_directory( self ):
    try:
      os.makedirs( os.path.join( self.commit_number, ".dbuild", "logs" ) )
    except:
      shutil.rmtree( self.commit_number )
      os.makedirs( os.path.join( self.commit_number, ".dbuild", "logs" ) )

  def __clone_repo_into_dir( self ):
    # Using direct commands instead of self.__exec_cmd as GIT uses the current working directory. Need to chdir.
    os.chdir( self.commit_number )

    try:
      subprocess.check_call( ["git", "init", "."] )
      subprocess.check_call( ["git", "remote", "add", "origin", self.git_url] )
      subprocess.check_call( ["git", "fetch", "origin"] )
      subprocess.check_call( ["git", "checkout", self.commit_number] )

      return {
        "status": 0,
        "error": "",
        "output": "Successfully checked out revision %s from GIT URL %s" % (self.commit_number, self.git_url)
      }
    except Exception as e:
      return {
        "status": -1,
        "error": str( e ),
        "output": ""
      }
    finally:
      os.chdir( ".." )

  def __build_ci_docker( self ):
    return self.__exec_cmd(
      "docker",
      "build",
      "-t", self.__docker_tag(),
      "-f", os.path.join( self.commit_number, "Dockerfile.ci" ),
      self.commit_number
    )

  def __run_ci_docker( self ):
    return self.__exec_cmd(
      "docker",
      "run",
      "--privileged",
      "-it",
      "--rm",
      "-v", os.abspath( self.commit_number ) + ":/project:rw",
      self.__docker_tag
    )

  def __docker_tag( self ):
    return "%s/%s:%s" % (self.group, self.app, self.commit_number)

  def __exec_cmd( self, *args ):
    log_number = uuid4().hex
    try:
      os.makedirs( os.path.join( self.commit_number, ".dbuild", "logs", log_number ) )
    except:
      pass
    error_log = os.path.join( self.commit_number, ".dbuild", "logs", log_number, "error.log" )
    output_log = os.path.join( self.commit_number, ".dbuild", "logs", log_number, "output.log" )

    with open( error_log, "w", encoding = "utf-8" ) as error_fh:
      with open( output_log, "w", encoding = "utf-8" ) as output_fh:
        output_fh.write( " ".join( args ) )
        output_fh.write( "\n" )

        status_code = subprocess.call( args, stderr = error_fh, stdout = output_fh, timeout = 3600 )

    with open( error_log, "r", encoding = "utf-8" ) as error_fh:
      with open( output_log, "r", encoding = "utf-8" ) as output_fh:
        return {
          "status": status_code,
          "error": error_fh.read(),
          "output": output_fh.read()
        }
