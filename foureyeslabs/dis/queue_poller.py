from concurrent.futures import ThreadPoolExecutor
from foureyeslabs.dis.build_job import BuildJob
from ostruct import OpenStruct
from uuid import uuid4

import boto3
import json
import time
import signal
import sys

MAX_WORKERS = 4

class Runner( object ):
  def __init__( self ):
    self.executor = ThreadPoolExecutor( max_workers = MAX_WORKERS )

  def run( self ):
    workers = [ QueuePoller() for i in range( 0, MAX_WORKERS ) ]
    processes = [ self.executor.submit( worker.run ) for worker in workers]
    signal.signal( signal.SIGTERM, self.signal_term_handler )

    while True:
      try:
        time.sleep( 5 )
      except KeyboardInterrupt:
        [worker.stop_working() for worker in workers]

  def signal_term_handler(self, signal, frame):
    [worker.stop_working() for worker in workers]

class QueuePoller( object ):
  def __init__( self ):
    self.sqs = boto3.resource( "sqs" )
    self.queue = self.sqs.get_queue_by_name( QueueName = "docker-ci-packager-start-build-queue" )
    self.s3 = boto3.resource( "s3" )
    self.bucket = self.s3.Bucket( "manheim-docker-ci-packager-response" )
    self.should_be_working = True

  def run( self ):
    while self.should_be_working:
      try:
        self.poll()
      except:
        pass # TODO make this understand the exception, and react accordingly

  def stop_working( self ):
    self.should_be_working = False

  def poll( self ):
    messages = self.queue.receive_messages( MaxNumberOfMessages = 1, WaitTimeSeconds = 10 )

    if len( messages ) > 0:
      for message in messages:
        body = message.body
        job_info = json.loads( body )
        message.delete()
        job = BuildJob( job_info )
        result = job.run()
        self.send_result( result )

  def send_result( self, result ):
    commit_number = result[ "commit_number" ]
    self.bucket.put_object(
      ACL = "private",
      Body = json.dumps( result ).encode( "utf-8" ),
      Key = commit_number
    )

if __name__ == "__main__":
  while True:
    try:
      runner = Runner()
      runner.run()
    except:
      pass # TODO notify something went seriously wrong
