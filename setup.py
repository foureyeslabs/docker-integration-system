from setuptools import setup, find_packages

setup(
  name = "Docker Integration System",
  version = "0.0.2",
  packages = find_packages(),
  install_requires = ['ostruct~=2.1.2', 'boto3~=1.3.1'],

  author = "Rob Taylor",
  author_email = "rtaylor@foureyeslabs.com",
  description = "A continuous integration and deployment system, that leverages Docker for each stage."
)
