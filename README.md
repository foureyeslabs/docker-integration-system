# Docker Integration System

This system relies on convention to start the process, then relies on Docker runners to complete it.

## Dockerfile.ci

Each project needs to have one particular file.

`Dockerfile.ci`

This file, in the root folder, will be run for the build, test, publish phases. It should all be within this one file.
The descriptors of how the application is actually provisioned and deployed is another matter, and not included in
this Docker container.
